import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import pairwise_distances
import re
import Levenshtein


def choose_fmt(fmt, typ):
    if fmt=='default':
        if typ=='yearmonth':
            return '%Y%m'
        elif typ in ['date','datetime']:
            return "%d%b%Y:%H:%M:%S"
        elif typ=='year':
            return '%Y'
    return fmt

def gather_date_vars(var_schemas):
    date_types = ['date','yearmonth','datetime','year']
    df = pd.DataFrame([],columns=['var','table','desc'])
    for table, vars in var_schemas.items():
        for var, desc in vars.items():
            if desc['type'] in date_types:
                df = df.append({'var':desc['name'],
                                'table':table,
                                'desc': desc['description'],
                                'type':desc["type"],
                               'format':choose_fmt(desc["format"],desc["type"])},
                               ignore_index=True)

    return df

def find_matchings_start_end(df):

    matching_df_list = []
    match_start_end = {"début":"fin","départ":"arrivée","entrée":"sortie","ancien":"nouveau"}
    start_regexp = re.compile('|'.join(match_start_end.keys()))
    end_regexp = re.compile('|'.join(match_start_end.values()))
    for table in df['table'].unique():
        df_table = df[(df['table']==table)]
        for ty in df_table['type'].unique():
            df_table_type = df_table[df_table['type']==ty]
            if df_table_type.shape[0]>1:
                df_table_type_start  = df_table_type[df_table_type['desc'].str.contains(start_regexp)]
                df_table_type_end  = df_table_type[df_table_type['desc'].str.contains(end_regexp)]
                if df_table_type_start.shape[0]>0 and df_table_type_end.shape[0]>0:
                    matches = []
                    matches_desc = []
                    matches_format = []
                    matches_distances = []
                    #compute levenshtein distance on var name
                    leven_distances = np.array([[Levenshtein.distance(df_table_type_start.iloc[i]['var'].upper(),
                                                                      df_table_type_end.iloc[j]['var'].upper())
                                                 for j in range(df_table_type_end.shape[0])]
                                                for i in range(df_table_type_start.shape[0])]
                                              )
                    #leven_distances = leven_distances/np.max(leven_distances)
                    #matching on composite distance
                    indices = leven_distances.argmin(axis=1)
                    distances = leven_distances[np.arange(len(indices)),indices]
                    matches += [df_table_type_end['var'].iloc[i] for i in indices]
                    matches_desc += [df_table_type_end['desc'].iloc[i] for i in indices]
                    matches_format += [df_table_type_end['format'].iloc[i] for i in indices]
                    matches_distances += list(distances)
                    #update df_table_type_start with matches
                    df_table_type_start['match'] = matches
                    df_table_type_start['match_desc'] = matches_desc
                    df_table_type_start['match_format'] = matches_format
                    df_table_type_start['distance'] = matches_distances
                    #store in list of matching df
                    matching_df_list.append(df_table_type_start)
    if len(matching_df_list)>0:
        #concatenate matching df
        df_matchings = pd.concat(matching_df_list)
        #select relevant matchings (the thresholds are specifically designed for SNDS)
        df_matchings = df_matchings[df_matchings['distance'].isin([1,2,3])]
    else:
        df_matchings = pd.DataFrame([],
                columns = ['var','table','desc','match','match_desc','distance'])
    return df_matchings



def find_matchings_yearmonth(df):
    """
    Construit la table de matchings entre les dates au format mois+année et les dates au format jour+mois+année
    arguments
    ---------
    df: pd.DataFrame contenant le nom des variables, leur type et leur description

    returns
    -------
    df_ym: pd.DataFrame contenant les variables et leurs matches éventuels
    """
    matches = []
    matches_desc = []
    matches_format = []
    matches_distances = []
    df_ym = df[df['type']=='yearmonth']
    for table in df['table'].unique():
        df_table = df[(df['table']==table)]
        df_table_ym = df_table[df_table['type']=='yearmonth']
        df_table_date = df_table[df_table['type']=='date']
        if (df_table_ym.shape[0]>0):
            if (df_table_date.shape[0]>0):
                #compute levenshtein distance on var name
                leven_distances = np.array([[Levenshtein.distance(df_table_ym.iloc[i]['var'].upper(),
                                                                  df_table_date.iloc[j]['var'].upper()) for j in range(
                    df_table_date.shape[0])]
                                            for i in range(df_table_ym.shape[0])])
                leven_distances = leven_distances/np.max(leven_distances)
                #compute distance on description
                cv = CountVectorizer()
                desc_encoded = cv.fit_transform(df_table['desc'])
                desc_ym_encoded = cv.transform(df_table_ym['desc'])
                desc_date_encoded = cv.transform(df_table_date['desc'])
                desc_distances = pairwise_distances(desc_ym_encoded.toarray(),desc_date_encoded.toarray(),metric='matching')
                #match on composite distance
                pair_distances = desc_distances + leven_distances
                #matching on composite distance
                indices = pair_distances.argmin(axis=1)
                distances = pair_distances[np.arange(len(indices)),indices]
                matches += [df_table_date['var'].iloc[i] for i in indices]
                matches_desc += [df_table_date['desc'].iloc[i] for i in indices]
                matches_format += [df_table_date['format'].iloc[i] for i in indices]
                matches_distances += list(distances)
                if not indices.shape[0]==df_table_ym.shape[0]:
                    print(df_table_ym.shape)
                    print(df_table_date.shape)
            else:
                matches+=df_table_ym.shape[0]*['']
                matches_desc+=df_table_ym.shape[0]*['']
                matches_format+=df_table_ym.shape[0]*['']
                matches_distances+=df_table_ym.shape[0]*[100]


    df_ym['match'] = matches
    df_ym['match_desc'] = matches_desc
    df_ym['match_format'] = matches_format
    df_ym['distance'] = matches_distances

    #retirer tous les mauvais matches
    df_ym = df_ym[df_ym.match!='']
    df_ym = df_ym[~(df_ym['var'].isin(['PRS_DRA_AME','BEN_DRA_AME']))]
    df_ym = df_ym[~(df_ym['table'].isin(['MA_REM_FT','OPEN_DAMIR']))]

    return df_ym

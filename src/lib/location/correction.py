import pandas as pd
import numpy as np
from itertools import product

def correct_etanum_corres(df,finess_nomenc):

    mapping_gj = {jur:geo  for jur,geo in zip(finess_nomenc['ej_finess'],finess_nomenc['et_finess'])}
    etanum_j_list = ["ETA_NUM","ETA_NUM_JUR"]
    etanum_geo_list = ["ETA_NUM_GEO","ETA_NUM_TWO"]
    for etanum_j,etanum_geo in product(etanum_j_list,etanum_geo_list):
        if etanum_j in df.columns and etanum_geo in df.columns:
            df[etanum_geo] = df[etanum_j].map(mapping_gj)

    return df

def correct_etanum_socrai(df,finess_nomenc):

    mapping = {etanum_j:socrai for etanum_j,socrai in zip(finess_nomenc['ej_finess'],finess_nomenc['ej_rs'])}
    if "ETA_NUM" in df.columns and "SOC_RAI" in df.columns:
        df["SOC_RAI"] = df["ETA_NUM"].map(mapping)

    return df

def correct_dpt_com(df,geo_nomenc):

    def select_random_com(dpt):
        dpt_coms = geo_nomenc[geo_nomenc['GEO_DPT_COD']==dpt]['GEO_COM_COD']
        com = np.random.choice(dpt_coms.values,1)
        return com[0]

    if 'BEN_RES_DPT' in df.columns and 'BEN_RES_COM' in df.columns:
        df['BEN_RES_DPT'] = df['BEN_RES_DPT'].astype(str)
        df['BEN_RES_COM'] = df['BEN_RES_DPT'].apply(select_random_com)
    return df

def make_all_location_corrections(df,finess_nomenc,geo_nomenc):

    df = correct_dpt_com(df,geo_nomenc)
    df = correct_etanum_corres(df,finess_nomenc)
    df = correct_etanum_socrai(df,finess_nomenc)

    return df

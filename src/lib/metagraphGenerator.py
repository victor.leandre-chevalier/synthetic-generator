from .graphGenerator import GraphGenerator
from .dates.matching import find_matchings_start_end, find_matchings_yearmonth, gather_date_vars
from networkx import connected_components
import re
import os
import json
import pandas as pd

class MetaGraphGenerator():
    """
    Coordinator for several graph generators. In particular, it handles the schemes
    where there are several connected components in the graph, and builds the subsequent graph generators.
    """

    def __init__(self,
                graph,
                links,
                base_name,
                roots,
                separator):
        """
        Parameters
        ----------
        graph: networkx.Graph, graph which represents the whole database to be generated
        links: pandas.DataFrame, table which contains all the foreign keys linking tables together
        base_name: str, ex "SNDS"
        roots: str or list, the root of the unique connected component or the list of roots of each connected component
        """
        self.base_name = base_name
        self.separator = separator
        components = connected_components(graph)
        components = [c for c in components] #to prevent the generator object from behaving weirdly
        if isinstance(roots, str):
            roots = [roots]
        n_components = len([c for c in components])
        n_roots = len(roots)
        #count the number of components with 3 tables or more
        n_complex_components = len([c for c in components if len(c)>2])

        if n_roots != n_components and n_complex_components != n_roots:
            raise ValueError(f"Les nombres de composantes connectées de cardinal > 2 ({n_complex_components}) et de racines ({n_roots}) ne concordent pas.")

        self.subgenerators = []
        for c in components:
            sub_graph = graph.subgraph(c)
            if len(c)>2 or n_roots == n_components:
                #choose the root that exists in the component among those given by the user
                root = list(set(roots).intersection(sub_graph))[0]

            else:
                #randomly choose the root
                root = list(c)[0]
            self.subgenerators.append(GraphGenerator(sub_graph,links,base_name,root,self.separator))
        self.subgenerators_built = False

    def build_var_schemas(self,path2schema,schema_modifiers=[]):
        """
        Build a dictionary of the form dict[table][variable] = variable description, from the given schemas.
        Essentially, it avoids having to reopen schema files on the fly.

        Parameters
        ----------
        path2schema: str, path to the folder containing the json files which describe the database schema.
        """
        #read the .json files

        self.var_schemas = {}
        #store default formats for SNDS
        if self.base_name=='SNDS':
            default_formats = {
                'datetime':'%d%b%Y:%H:%M:%S',
                'date':'%d%b%Y:%H:%M:%S',
                'yearmonth':'%Y%m',
                'year':'%Y',
                'month':'%m'
            }
        for root, dirs_list, files_list in os.walk(path2schema):
            for file_name in files_list:
                if os.path.splitext(file_name)[-1] == '.json':
                    file_name_path = os.path.join(root, file_name)
                    desc = json.load(open(file_name_path,encoding='utf-8'))
                    table_name = file_name.split('.')[0]
                    self.var_schemas[table_name] = {}
                    for field in desc['fields']:
                        if field['format']=='default' \
                        and self.base_name=='SNDS' \
                        and (field['type'] in default_formats): 
                            field['format'] = default_formats[field['type']]
                        self.var_schemas[table_name][field['name']] = field
        for sm in schema_modifiers:
            sm.run(self.var_schemas)


    def build_nomenclatures(self,path2nomenc):
        """
        Build a dictionary of the form dict[nomenclature] = pd.DataFrame
        Parameters
        ----------
        path2nomenc: str, path to the folder which contains nomenclatures under csv format with ';' separator.
        """

        self.nomenclatures = {}

        for root,dirs_list,files_list in os.walk(path2nomenc):
            for file_name in files_list:
                if os.path.splitext(file_name)[-1]=='.csv':
                    file_name_path = os.path.join(root, file_name)
                    if 'IR_GEO_V' in file_name:
                        df_nomenc = pd.read_csv(file_name_path,sep=';',dtype={'GEO_DPT_COD':str,'GEO_COM_COD':str})
                    elif 'FINESS' in file_name:
                        df_nomenc = pd.read_csv(file_name_path,sep=';',dtype='string')
                        df_nomenc.drop_duplicates(subset='ej_finess',inplace=True)
                        df_nomenc['et_finess'].fillna('',inplace=True)

                    else:
                        df_nomenc = pd.read_csv(file_name_path,sep=';')
                    self.nomenclatures[file_name.split('.')[0]] = df_nomenc

    def build_table_path(self,path2schema):
        """
        Build a dictionary of the form dict[table] = where/to/write/the/table
        The idea is to export the table (the csv file) in the same folder structure as the json files.
        Parameters
        ----------
        path2schema: str, path to the folder containing schemas files.
        """
        self.table_path = {}
        for root, dirs_list, files_list in os.walk(path2schema):
            for file_name in files_list:
                if os.path.splitext(file_name)[-1] == '.json':
                    file_name_path = os.path.join(root.replace(path2schema,''), file_name)
                    table_name = file_name.split('.')[0]
                    if file_name_path[0] in ['\\','/']:
                        file_name_path = file_name_path[1:]
                    self.table_path[table_name] = file_name_path.replace('json','csv')


    def build_dates_matching(self,path2schema):
        """
        Build the match between variables in the same table to define a chronological order between them.
        Only relevant if base_name is 'SNDS'.

        Parameters
        ----------
        path2schema: str, path to the folder containing schemas files.
        """

        self.df_dates = gather_date_vars(self.var_schemas)
        self.matching_start_end = find_matchings_start_end(self.df_dates)
        self.matching_ym = find_matchings_yearmonth(self.df_dates)

    def push_to_subgenerators(self):
        """
        Push parameters that are valid for the whole base to each of the subgenerators
        """
        for subgenerator in self.subgenerators:
            subgenerator.var_schemas = self.var_schemas
            subgenerator.nomenclatures = self.nomenclatures
            subgenerator.table_path = self.table_path
            subgenerator.df_dates = self.df_dates
            subgenerator.matching_start_end = self.matching_start_end
            subgenerator.matching_ym = self.matching_ym
        self.subgenerators_built = True

    def sample(self,n_beneficiaires,path2output=''):
        """
        Call the sample method on all the subgenerators of the Metagenerator.
        Can be parallelized.

        Parameters
        ----------
        n_beneficiaires: int, number of patients to include in the database.
        path2output: str, folder where to export the tables once generated.
        """
        if not self.subgenerators_built:
            self.push_to_subgenerators()

        for subgenerator in self.subgenerators:
            subgenerator.sample(n_beneficiaires,path2output)

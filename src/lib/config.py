import configparser
import json
from pathlib import Path
import os
from .schema_modifier import SchemaModifier

class Config():
    """
    Read the config file and create the absolute paths from it.
    Also handles the typing (converting strings to lists) and creates the schema modifiers.
    """

    config = configparser.ConfigParser()
    #folder one level higher than the "bases" folder
    WORKPATH = Path(__file__).resolve().parent.parent.parent

    def __init__(self,path2config):
        """
        Parameters
        ----------
        path2config: str, path to the configuration file
        """

        self.config.read(path2config)

        self.path2resources = (os.path.join(self.WORKPATH,self.config['BASE']['path2resources'])
                               if 'path2resources' in self.config['BASE']
                               else os.path.join(self.WORKPATH,'src/resources')
                               )
        self.path2schemas = os.path.join(self.path2resources,'schemas')
        self.path2nomenc = os.path.join(self.path2resources,'nomenclatures')
        if not os.path.exists(self.path2schemas):
            raise ValueError('Le dossier resources ne contient pas de dossier "schemas".')
        if not os.path.exists(self.path2nomenc):
            raise ValueError('Le dossier resources ne contient pas de dossier "nomenclatures".')
        self.path2export = os.path.join(self.WORKPATH,self.config['BASE']['export_path'])
        self.base_name = self.config['BASE']['base_name']
        self.separator = self.config['BASE']['sep']
        #roots is a list, so convert to a json readable format prior to loading it with json
        if "[" in self.config['BASE']['roots']:
            self.roots = json.loads(self.config['BASE']['roots'].replace("[","[\"").replace("]","\"]").replace(",","\",\""))
        else:
            self.roots = self.config['BASE']['roots']
        if 'n_beneficiaires' in self.config['BASE']:
            self.n_beneficiaires = int(self.config['BASE']['n_beneficiaires'])
        elif 'volume_beneficiaires' in self.config['BASE'] and self.base_name == "SNDS":
            self.n_beneficiaires = int(float(self.config['BASE']['volume_beneficiaires']) * 20 / 3)

        if 'SCHEMA MODIFIER' in self.config:
            if len(list(self.config['SCHEMA MODIFIER'].keys()))>0:
                self.schema_modifiers = [SchemaModifier(v) for v in self.config['SCHEMA MODIFIER'].values()]
            else:
                self.schema_modifiers = []

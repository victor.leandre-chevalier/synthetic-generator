# Synthetic Generator

Generateur de données synthétiques

## Présentation

Ce package permet de créer des données de synthèse pour une base de données relationnelle qui peut être représentée sous forme d'arbre. Le SNDS correspond notamment à ce format, et le générateur a été initialement créé pour générer un SNDS synthétique. Toutefois, il peut être utilisé pour d'autres bases, et nous travaillons nous-mêmes à cette adaptation. 

## Structure du générateur

Essentiellement, le package est composé des modules suivants:
- [randomGenerator](src/lib/randomGenerator.py) qui permet de générer une colonne aléatoire suivant un type et des contraintes ou une nomenclature donnée
- [graphGenerator](src/lib/graphGenerator.py) qui permet de gérer l'ordre dans lequel les tables sont générées au sein d'une composante connectée de la base, et qui appelle randomGenerator pour chacune des colonnes. A noter que c'est aussi ce module qui se charge de faire respecter les clés de jointures entre les tables.
- [MetaGraphGenerator](src/lib/metagraphGenerator.py) qui permet de gérer plusieurs graphGenerator (un par composante connectée de la base).

## Utilisation

Pour utiliser ce package, deux éléments sont nécessaires:
- un dossier de [ressources](src/resources) contenant lui-même:
    - un [schéma formel](src/resources/schemas) de la base à générer
    - les [nomenclatures](src/resources/nomenclatures) correspondantes
    - (optionnel) un fichier [links.csv](src/resources/links.csv) qui regroupe tous les liens entre les tables. Si ce fichier est absent, il sera généré à partir du schéma, en revanche s'il est présent ce sera lui qui sera utilisé pour la génération, même si il n'est pas congruent avec le schéma formel
    - (optionnel) un fichier [graph.pkl](src/resources/graph.pkl) qui contient un graphe représentant la base de données, lui-même construit à partir du fichier **links.csv**. Si ce fichier est absent, il sera généré à partir du fichier **links.csv** (et si ce dernier est également absent, il sera construit à partir du schéma formel puis le graphe en sera déduit). En revanche, si **graph.pkl** est présent, ce sera lui qui sera utilisé pour la génération, même s'il n'est pas congruent avec le schéma formel ou avec le fichier **links.csv**.
- un [fichier de configuration](src/conf/snds.config)

Les éléments qui sont fournis avec le package sont les éléments par défaut pour le SNDS. Si l'utilisateur souhaite utiliser son propre dossier de ressources, il suffit de remplacer le dossier déjà présent dans le package, ou bien de modifier le champ `path2resources` du fichier de configuration pour qu'il pointe vers le nouveau dossier de ressources (celui présent dans le package par défaut sera alors ignoré).  

Ensuite, il suffit :

- Installer les dépendances

         pip3 install -r requirements.txt

- Effectuer la commande:

         python3 src/generate_data.py --config <path to configuration file>

### Configuration du générateur

Dans le fichier de configuration, on trouve les champs suivants:

**Section "BASE"**:

- **base_name**: le nom de la base. A heure, Seuls les noms "SNDS" et "OSCOUR" déclenchent un comportement particulier.
- **roots**: le nom des tables qui servent de racines à la structure de la base, sous forme de liste (eg [IR_BEN_R,KI_CCD_R]). Dans le cas du SNDS, il s'agit de tables comme IR_BEN_R. Dans le cas où la base est composée de plusieurs ensembles de tables non connectés, il n'est pas obligatoire de renseigner la racine pour les ensembles de moins de deux tables.
- **n_beneficiaires**: le nombre de lignes dans les tables racines. Dans le cas du SNDS, il s'agit du nombre de bénéficiaires.
- **volume_beneficiaires**: (WIP)
- **export_path**: emplacement où les fichiers csv seront créés.
- **path2resources**: (optionnel) emplacement du dossier dans lequel se trouve un dossier "nomenclatures" et un dossier "schemas" (et éventuellement un fichier links.csv et un fichier graph.pkl)

**Section "SCHEMA MODIFIER"**:

- le nom des clés importe peu, ce qui sera lu sera uniquement ce qui est après le signe égal ("="). Cette section permet de faire des amendements au schéma des tables qui ne seront pris en compte que pour la génération (ie, les fichiers d'origine des schémas ne seront pas modifiés).

Le format attendu pour les modificateurs du schéma est: `modifier_name=<table>|<variable>|<propriété>|<nouvelle valeur>`.

Par exemple:

- `id_modifier=IR_BEN_R|BEN_NIR_PSA|length|4` changera dans le schéma de la table IR_BEN_R la longueur du champ BEN_NIR_PSA à 4. A noter qu'il est possible d'indiquer une globstring plutôt qu'un nom de table, de variable ou de propriété.

- `date_modifier=*|MY_DATE|type|datetime` changera le type de la variable MY_DATE à datetime dans toutes les tables dans lesquelles ce champs est présent.
